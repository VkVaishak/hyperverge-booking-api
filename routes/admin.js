const express = require("express");
const router = express.Router();
const adminLib = require("../lib/admin");

router.patch("/login", adminLib.loginUser);

module.exports = router;
