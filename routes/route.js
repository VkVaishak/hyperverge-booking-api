const express = require("express");
const router = express.Router();
const routesLib = require("../lib/routes");
const isAdmin = require("../middleware/isAdmin");

router.post("/", isAdmin, routesLib.createRoute);
router.get("/", routesLib.getRoute);
router.get("/seat", routesLib.getSeatDetails);

module.exports = router;
