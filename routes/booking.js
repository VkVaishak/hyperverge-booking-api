const express = require("express");
const router = express.Router();
const bookingLib = require("../lib/booking");
const isAdmin = require("../middleware/isAdmin");

router.post("/", bookingLib.createBooking);
router.get("/:id", bookingLib.getBooking);
router.delete("/cancel", bookingLib.cancelBooking);
router.delete("/reset", isAdmin, bookingLib.resetBookings);

module.exports = router;
