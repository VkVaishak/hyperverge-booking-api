require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const routeRoutes = require("./routes/route");
const bookingRoutes = require("./routes/booking");
const adminRoutes = require("./routes/admin");

const app = express();
const bodyParser = require("body-parser");

app.use(morgan(":method :url :status :date[iso] :response-time ms"));

app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Credentials", "true");
	res.header(
		"Access-Control-Allow-Methods",
		"GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS"
	);
	res.header(
		"Access-Control-Allow-Headers",
		"Accept, Authorization, Content-Type"
	);
	if (req.method === "OPTIONS") {
		return res.send(200);
	} else {
		return next();
	}
});

app.use(bodyParser.json({ limit: "10mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));

app.use("/api/route", routeRoutes);
app.use("/api/booking", bookingRoutes);
app.use("/api/admin", adminRoutes);

app.use("/status", (req, res) => {
	return res.status(200).send({
		status: true,
		message: "Server is up!",
	});
});
app.use((err, req, res, next) => {
	return res
		.status(err.status || 500)
		.send({ status: false, response: err.message || err });
});

module.exports = app;
