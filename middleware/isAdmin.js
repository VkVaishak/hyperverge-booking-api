const e = require("express");

const hasToken = require("../helpers/jwt").hasToken;
module.exports = async (req, res, next) => {
	try {
		const payload = hasToken(req);
		if (!payload) next({ status: 401, message: "Invalid token" });
		if (payload.user_type == "ADMIN") next();
		else next({ status: 403, message: "Forbidden" });
	} catch (err) {
		console.log(err);
		return next(err);
	}
};
