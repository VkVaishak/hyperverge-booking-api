const createBookingSchema = require("./createBooking.json");
const cancelBookingSchema = require("./cancelBooking.json");

module.exports = {
	createBookingSchema,
	cancelBookingSchema,
};
