const mongoose = require("mongoose");
const constants = require("../../constants");

// Generate Enum values for seat status
const seatStatusEnum = [];
Object.keys(constants.seatStatus).forEach((status) => {
	seatStatusEnum.push(constants.seatStatus[status]);
});

const routeSchema = new mongoose.Schema({
	number_of_seats: {
		type: Number,
		required: true,
	},
	seats: {
		required: true,
		type: [
			{
				seat_number: {
					type: Number,
					required: true,
				},
				status: {
					type: String,
					required: true,
					default: constants.seatStatus.available,
					enum: seatStatusEnum,
				},
			},
		],
	},
});

module.exports = mongoose.model("Route", routeSchema);
