const mongoose = require("mongoose");

const bookingSchema = new mongoose.Schema({
	user_name: {
		type: String,
		required: true,
	},
	email: {
		type: String,
		required: true,
	},
	contact: {
		type: String,
		required: true,
	},
	route_id: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Route",
	},
	seat_number: {
		type: Number, // 1 ticket per seat. One booking can contain only one seat
		ref: "SeatLayout",
	},
});

module.exports = mongoose.model("Booking", bookingSchema);
