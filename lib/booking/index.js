const bookingRepo = require("./bookingRepo");
const validator = require("../../schema/validator");
const {
	createBookingSchema,
	cancelBookingSchema,
} = require("../../schema/files/booking");

/**
 * @function createBooking
 * @param {Request} req
 * @param {Response} res
 * @returns bookingId
 */
const createBooking = async (req, res) => {
	try {
		const bookingData = req.body;
		const valid = validator.validate(createBookingSchema, bookingData);
		if (valid.status) {
			const bookingRes = await bookingRepo.createBooking(
				bookingData.seat_number,
				bookingData.user_name,
				bookingData.email,
				bookingData.contact
			);

			return res.status(201).send({
				status: true,
				response: {
					message: "Booking created successfully",
					id: bookingRes._id,
				},
			});
		} else {
			return res
				.status(400)
				.send({ status: false, response: valid.message });
		}
	} catch (err) {
		console.log(err);
		return res
			.status(err.status || 500)
			.send({ status: false, response: err.message || err });
	}
};

/**
 * @function cancelBooking
 * @param {Request} req
 * @param {Response} res
 * @returns none
 */
const cancelBooking = async (req, res) => {
	try {
		const bookingData = req.body;
		const valid = validator.validate(cancelBookingSchema, bookingData);
		if (valid.status) {
			await bookingRepo.deleteBooking(bookingData.seat_number);

			return res.status(204).send();
		} else {
			return res
				.status(400)
				.send({ status: false, response: valid.message });
		}
	} catch (err) {
		console.log(err);
		return res
			.status(err.status || 500)
			.send({ status: false, response: err.message || err });
	}
};

/**
 * @function resetBookings
 * @param {Request} req
 * @param {Response} res
 * @returns none
 */
const resetBookings = async (req, res) => {
	try {
		await bookingRepo.deleteBooking();
		return res.status(204).send();
	} catch (err) {
		console.log(err);
		return res
			.status(err.status || 500)
			.send({ status: false, response: err.message || err });
	}
};

/**
 * @function getBooking
 * @param {Request} req
 * @param {Response} res
 * @returns bookingDetails
 */
const getBooking = async (req, res) => {
	try {
		const { id } = req.params;
		const bookingRes = await bookingRepo.getBooking({
			_id: id,
		});
		if (!bookingRes || bookingRes.length == 0)
			return res.status(404).send({
				status: false,
				response: "Booking does not exist",
			});
		return res.status(200).send({
			status: true,
			response: bookingRes[0],
		});
	} catch (err) {
		console.log(err);
		return res
			.status(err.status || 500)
			.send({ status: false, response: err.message || err });
	}
};

module.exports = {
	createBooking,
	getBooking,
	cancelBooking,
	resetBookings,
};
