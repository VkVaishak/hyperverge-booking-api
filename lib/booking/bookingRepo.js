const Booking = require("../../db/models/booking");
const routeRepo = require("../routes/routesRepo");
const constants = require("../../constants");
/**
 * @description Creates a booking and save the seat.
 * @param {Number} seatNumber
 * @param {String} userName
 * @param {String} email
 * @param {String} contactNumber
 */
const createBooking = async (seatNumber, userName, email, contactNumber) => {
	try {
		const routeDetails = await routeRepo.getRoute();
		const seatDetails = routeDetails.seats.filter(
			(seat) => seat.seat_number == seatNumber
		)[0];
		if (!seatDetails || seatDetails.length == 0)
			throw { status: 404, message: "Seat does not exist" };
		if (seatDetails.status == constants.seatStatus.booked)
			throw { status: 400, message: "Seat is already booked" };

		const bookingSchema = new Booking({
			user_name: userName,
			email,
			contact: contactNumber,
			route_id: routeDetails._id,
			seat_number: seatNumber,
		});
		const bookingData = await bookingSchema.save();
		await routeRepo.updateSeat(constants.seatStatus.booked, seatNumber);
		return bookingData;
	} catch (err) {
		throw err;
	}
};

/**
 * @description Get Booking by query
 * @param {Object} query
 */
const getBooking = async (query) => {
	try {
		const bookingData = await Booking.find(query);
		return bookingData;
	} catch (err) {
		throw err;
	}
};

const deleteBooking = async (seatNumber) => {
	try {
		const query = {};
		if (seatNumber) query["seat_number"] = Number(seatNumber);
		await Booking.deleteMany(query);
		await routeRepo.updateSeat(constants.seatStatus.available, seatNumber);
	} catch (err) {
		throw err;
	}
};

module.exports = {
	createBooking,
	getBooking,
	deleteBooking,
};
