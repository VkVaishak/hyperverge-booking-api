process.env.NODE_ENV = "test";

const expect = require("chai").expect;
const request = require("supertest");

const app = require("../../app");
const db = require("../../db");
const Booking = require("../../db/models/booking");
const Route = require("../../db/models/routes");

describe("POST /api/booking", function () {
	before((done) => {
		db()
			.then(() => {
				return request(app).patch("/api/admin/login").send({
					email: "test@gmail.com",
					password: "123",
				});
			})
			.then((res) => {
				this.token = res.body.response.token;
				done();
			})
			.catch((err) => done(err));
	});

	beforeEach((done) => {
		Booking.deleteMany()
			.then(() => Route.deleteMany())
			.then(() => {
				return request(app)
					.post("/api/route")
					.set("Authorization", this.token);
			})
			.then(() => done())
			.catch(() => done(err));
	});

	it("Success, create a new booking", (done) => {
		request(app)
			.post("/api/booking")
			.send({
				seat_number: 20,
				user_name: "Vaishak",
				email: "testemail@test.com",
				contact: "91845359345",
			})
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(true);
				expect(body).to.have.nested.property("response.id");
				expect(statusCode).to.equal(201);

				done();
			})
			.catch((err) => done(err));
	});

	it("Fail, booking same seat again", (done) => {
		request(app)
			.post("/api/booking")
			.send({
				seat_number: 20,
				user_name: "Vaishak",
				email: "testemail@test.com",
				contact: "91845359345",
			})
			.then(() => {
				return request(app).post("/api/booking").send({
					seat_number: 20,
					user_name: "Vaishak",
					email: "testemail@test.com",
					contact: "91845359345",
				});
			})
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(false);
				expect(statusCode).to.equal(400);

				done();
			})
			.catch((err) => done(err));
	});
	it("Fail, booking an invalid seat", (done) => {
		request(app)
			.post("/api/booking")
			.send({
				seat_number: 80,
				user_name: "Vaishak",
				email: "testemail@test.com",
				contact: "91845359345",
			})
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(false);
				expect(statusCode).to.equal(404);
				done();
			})
			.catch((err) => done(err));
	});
	it("Fail, when mandatory parameters are not passed", (done) => {
		request(app)
			.post("/api/booking")
			.send({
				seat_number: 20,
				email: "testemail@test.com",
				contact: "91845359345",
			})
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(false);
				expect(statusCode).to.equal(400);
				expect(
					body.response.map((e) => e.params.missingProperty)
				).to.include("user_name");
				done();
			})
			.catch((err) => done(err));
	});
	it("Fail, when invalid email is passed", (done) => {
		request(app)
			.post("/api/booking")
			.send({
				seat_number: 20,
				email: "testemailtestcom",
				contact: "91845359345",
				user_name: "Test",
			})
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(false);
				expect(statusCode).to.equal(400);
				expect(body.response.map((e) => e.params.format)).to.include(
					"email"
				);
				done();
			})
			.catch((err) => done(err));
	});
});

describe("GET /api/booking/:id", function () {
	before((done) => {
		db()
			.then(() => {
				return request(app).patch("/api/admin/login").send({
					email: "test@gmail.com",
					password: "123",
				});
			})
			.then((res) => {
				this.token = res.body.response.token;
				done();
			})
			.catch((err) => done(err));
	});

	beforeEach((done) => {
		Booking.deleteMany()
			.then(() => Route.deleteMany())
			.then(() => {
				return request(app)
					.post("/api/route")
					.set("Authorization", this.token);
			})
			.then(() => done())
			.catch(() => done(err));
	});

	it("Success, get booking details", (done) => {
		request(app)
			.post("/api/booking")
			.send({
				seat_number: 20,
				user_name: "Vaishak",
				email: "testemail@test.com",
				contact: "91845359345",
			})
			.then((res) => {
				return request(app).get(`/api/booking/${res.body.response.id}`);
			})
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(true);
				expect(body).to.have.nested.property("response._id");
				expect(statusCode).to.equal(200);

				done();
			})
			.catch((err) => done(err));
	});

	it("Fail, get invalid booking details", (done) => {
		request(app)
			.get(`/api/booking/5f59aebc2aa70bbc929972b8`)
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(false);
				expect(statusCode).to.equal(404);

				done();
			})
			.catch((err) => done(err));
	});
});

describe("DELETE /api/booking/cancel", function () {
	before((done) => {
		db()
			.then(() => {
				return request(app).patch("/api/admin/login").send({
					email: "test@gmail.com",
					password: "123",
				});
			})
			.then((res) => {
				this.token = res.body.response.token;
				done();
			})
			.catch((err) => done(err));
	});

	beforeEach((done) => {
		Booking.deleteMany()
			.then(() => Route.deleteMany())
			.then(() => {
				return request(app)
					.post("/api/route")
					.set("Authorization", this.token);
			})
			.then(() =>
				request(app).post("/api/booking").send({
					seat_number: 20,
					user_name: "Vaishak",
					email: "testemail@test.com",
					contact: "91845359345",
				})
			)
			.then(() => done())
			.catch(() => done(err));
	});

	it("Success, Cancel booking", (done) => {
		request(app)
			.delete(`/api/booking/cancel`)
			.send({ seat_number: 20 })
			.then((res) => {
				const statusCode = res.statusCode;
				expect(statusCode).to.equal(204);
				return request(app).get("/api/route/seat?seat_number=20");
			})
			.then((res) => {
				const body = res.body;
				expect(body.response.seats[0].status).to.equal("AVAILABLE");
				done();
			})
			.catch((err) => done(err));
	});

	it("Success, Reset booking", (done) => {
		request(app)
			.delete(`/api/booking/reset`)
			.set("Authorization", this.token)
			.then((res) => {
				const statusCode = res.statusCode;
				expect(statusCode).to.equal(204);
			})
			.then(() => request(app).get("/api/route/seat?status=BOOKED"))
			.then((res) => {
				const body = res.body;
				expect(body.response).that.eql({});
				done();
			})
			.catch((err) => done(err));
	});
	it("Fails, Reset booking when auth token is not provided", (done) => {
		request(app)
			.delete(`/api/booking/reset`)
			.then((res) => {
				const statusCode = res.statusCode;
				expect(statusCode).to.equal(401);
				done();
			})
			.catch((err) => done(err));
	});
});
