process.env.NODE_ENV = "test";

const expect = require("chai").expect;
const request = require("supertest");

const app = require("../../app");
const db = require("../../db");

describe("PATCH /api/admin/login", function () {
	before((done) => {
		db()
			.then((res) => {
				done();
			})
			.catch((err) => done(err));
	});

	it("Success, logs in and returns token", (done) => {
		request(app)
			.patch("/api/admin/login")
			.send({
				email: "test@gmail.com",
				password: "123",
			})
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(true);
				expect(body).to.have.nested.property("response.token");
				expect(statusCode).to.equal(200);
				done();
			})
			.catch((err) => done(err));
	});
});
