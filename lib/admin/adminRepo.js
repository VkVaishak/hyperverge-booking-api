const jwt = require("../../helpers/jwt");

/**
 * @description Return JWT token with user type ADMIN
 * @param {String} email
 */
const login = (email) => {
	try {
		const payload = {
			email,
			user_type: "ADMIN",
		};
		const jwtToken = jwt.sign(payload);
		return jwtToken;
	} catch (err) {
		throw err;
	}
};

module.exports = {
	login,
};
