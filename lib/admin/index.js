const schema = require("../../schema/files/admin");
let { loginSchema } = schema;
const validator = require("../../schema/validator");
const adminRepo = require("./adminRepo");

/**
 * @function loginUser
 * @param {Request} req
 * @param {Response} res
 * @returns token
 */
const loginUser = async (req, res) => {
	try {
		const loginFields = req.body;
		const valid = validator.validate(loginSchema, loginFields);
		if (valid.status) {
			const loginResponse = await adminRepo.login(
				loginFields.email,
				loginFields.password
			);
			return res.status(200).send({
				status: true,
				response: {
					token: loginResponse,
				},
			});
		} else {
			return res
				.status(400)
				.send({ status: false, response: valid.message });
		}
	} catch (err) {
		console.log(err);
		return res
			.status(err.status || 500)
			.send({ status: false, response: err.message || err });
	}
};

module.exports = {
	loginUser,
};
