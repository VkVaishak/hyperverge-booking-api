process.env.NODE_ENV = "test";

const expect = require("chai").expect;
const request = require("supertest");

const app = require("../../app");
const db = require("../../db");
const Route = require("../../db/models/routes");

describe("POST /api/route", function () {
	before((done) => {
		db()
			.then(() => {
				return request(app).patch("/api/admin/login").send({
					email: "test@gmail.com",
					password: "123",
				});
			})
			.then((res) => {
				this.token = res.body.response.token;
				done();
			})
			.catch((err) => done(err));
	});

	beforeEach((done) => {
		Route.deleteMany()
			.then(() => done())
			.catch(() => done(err));
	});
	it("Success, create a new route", (done) => {
		request(app)
			.post("/api/route")
			.set("Authorization", this.token)
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(true);
				expect(body).to.have.nested.property("response.id");
				expect(statusCode).to.equal(201);

				done();
			})
			.catch((err) => done(err));
	});

	it("Fails, create a new route without auth token", (done) => {
		request(app)
			.post("/api/route")
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(false);
				expect(statusCode).to.equal(401);
				done();
			})
			.catch((err) => done(err));
	});

	it("Fail, multiple routes cannot be created", (done) => {
		request(app)
			.post("/api/route")
			.set("Authorization", this.token)
			.then(() => {
				return request(app)
					.post("/api/route")
					.set("Authorization", this.token);
			})
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(false);
				expect(body)
					.property("response")
					.to.equal("Route already exists");
				expect(statusCode).to.equal(400);
				done();
			})
			.catch((err) => done(err));
	});
});

describe("GET /api/route", function () {
	before((done) => {
		db()
			.then(() => {
				return request(app).patch("/api/admin/login").send({
					email: "test@gmail.com",
					password: "123",
				});
			})
			.then((res) => {
				this.token = res.body.response.token;
				done();
			})
			.catch((err) => done(err));
	});

	beforeEach((done) => {
		Route.deleteMany()
			.then(() => done())
			.catch((err) => done(err));
	});

	it("Success, get route", (done) => {
		request(app)
			.post("/api/route")
			.set("Authorization", this.token)
			.then(() => {
				return request(app).get("/api/route");
			})
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(true);
				expect(body).to.have.nested.property("response._id");
				expect(statusCode).to.equal(200);
				done();
			})
			.catch((err) => done(err));
	});

	it("Fail, when no route is available", (done) => {
		Route.deleteMany()
			.then(() => {
				return request(app).get("/api/route");
			})
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(false);
				expect(statusCode).to.equal(404);
				done();
			})
			.catch((err) => done(err));
	});
});

describe("GET /api/route/seat", function () {
	before((done) => {
		db()
			.then(() => {
				return request(app).patch("/api/admin/login").send({
					email: "test@gmail.com",
					password: "123",
				});
			})
			.then((res) => {
				this.token = res.body.response.token;
				done();
			})
			.catch((err) => done(err));
	});

	beforeEach((done) => {
		Route.deleteMany()
			.then(() => {
				return request(app)
					.post("/api/route")
					.set("Authorization", this.token);
			})
			.then(() => done())
			.catch((err) => done(err));
	});

	it("Success, get seat details", (done) => {
		request(app)
			.get("/api/route/seat?seat_number=20")
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(true);
				expect(statusCode).to.equal(200);

				expect(
					body.response.seats.map((e) => e.seat_number)
				).to.include(20);
				done();
			})
			.catch((err) => done(err));
	});
	it("Success, get available seats details", (done) => {
		request(app)
			.get("/api/route/seat?status=AVAILABLE")
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(true);
				expect(statusCode).to.equal(200);

				expect(body.response.seats.map((e) => e.status)).to.not.include(
					"BOOKED"
				);
				done();
			})
			.catch((err) => done(err));
	});

	it("Success, get booked seats details", (done) => {
		request(app)
			.post("/api/booking")
			.send({
				seat_number: 20,
				user_name: "Vaishak",
				email: "testemail@test.com",
				contact: "91845359345",
			})
			.then(() => request(app).get("/api/route/seat?status=BOOKED"))
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(true);
				expect(statusCode).to.equal(200);

				expect(body.response.seats.map((e) => e.status)).to.not.include(
					"AVAILABLE"
				);
				done();
			})
			.catch((err) => done(err));
	});

	it("Fail, when seat does not exist", (done) => {
		request(app)
			.get("/api/route/seat?seat_number=42")
			.then((res) => {
				const body = res.body;
				const statusCode = res.statusCode;
				expect(body).property("status").to.equal(true);
				expect(statusCode).to.equal(200);
				expect(body).property("response").that.eql({});
				done();
			})
			.catch((err) => done(err));
	});
});
