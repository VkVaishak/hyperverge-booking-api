const Route = require("../../db/models/routes");
const constants = require("../../constants");

/**
 * @description Creates a route and all the seats.
 * @description By default, only 40 seats can be provisioned in a route
 * @description Only one route exists - cannot create multiple routes
 */
const createRoute = async () => {
	try {
		const existingRoute = await getRoute();
		if (existingRoute)
			throw {
				status: 400,
				message: "Route already exists",
			};
		const maxSeats = constants.numberOfSeats;
		const seatLayout = createSeatLayout(maxSeats);
		const routeSchema = new Route({
			seats: seatLayout,
			number_of_seats: maxSeats,
		});
		const routeDetails = await routeSchema.save();
		return routeDetails;
	} catch (err) {
		throw err;
	}
};

/**
 * @param {Number} totalSeats
 * @description Returns an array of seats with seat number and status as available
 */
const createSeatLayout = (totalSeats) => {
	try {
		const seatLayout = [];
		for (let i = 0; i < totalSeats; i++) {
			seatLayout.push({
				seat_number: i + 1,
				status: constants.seatStatus.available,
			});
		}
		return seatLayout;
	} catch (err) {
		throw err;
	}
};

/**
 * @description Return route details based on the query passed
 * @param {Object} query
 */
const getRoute = async (query) => {
	try {
		const routeDate = await Route.findOne(query);
		return routeDate;
	} catch (err) {
		throw err;
	}
};

/**
 * @description Return route details based on the query passed
 * @param {Object} query
 */
const getSeatDetails = async (query) => {
	try {
		const routeDate = await Route.aggregate([
			{ $unwind: "$seats" },
			{ $match: query },
			{ $group: { _id: "$_id", seats: { $push: "$seats" } } },
		]);
		return routeDate[0];
	} catch (err) {
		throw err;
	}
};

/**
 * @description Updates a seat to the status provided
 * @param {Number} seatNumber
 * @param {String} status
 */
const updateSeat = async (status, seatNumber) => {
	try {
		const query = {};
		let seatStatus = "seats.$[].status";
		if (seatNumber) {
			query["seats.seat_number"] = seatNumber;
			seatStatus = "seats.$.status";
		}
		await Route.update(query, {
			$set: {
				[seatStatus]: status,
			},
		});
	} catch (err) {
		throw err;
	}
};

module.exports = {
	createRoute,
	getRoute,
	getSeatDetails,
	updateSeat,
};
