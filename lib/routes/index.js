const routeRepo = require("./routesRepo");

/**
 * @function createRoute
 * @param {Request} req
 * @param {Response} res
 * @returns routeId
 */
const createRoute = async (req, res) => {
	try {
		const routeRes = await routeRepo.createRoute();

		return res.status(201).send({
			status: true,
			response: {
				message: "Route created successfully",
				id: routeRes._id,
			},
		});
	} catch (err) {
		console.log(err);
		return res
			.status(err.status || 500)
			.send({ status: false, response: err.message || err });
	}
};

/**
 * @function getRoute
 * @param {Request} req
 * @param {Response} res
 * @returns route Details
 */
const getRoute = async (req, res) => {
	try {
		const routeRes = await routeRepo.getRoute();
		if (!routeRes)
			return res.status(404).send({
				status: false,
				response: "No routes found",
			});

		return res.status(200).send({
			status: true,
			response: routeRes,
		});
	} catch (err) {
		console.log(err);
		return res
			.status(err.status || 500)
			.send({ status: false, response: err.message || err });
	}
};

/**
 * @function getSeatStatus
 * @param {Request} req
 * @param {Response} res
 * @returns seat Details
 */
const getSeatDetails = async (req, res) => {
	try {
		const { query } = req;
		dbQuery = {};

		// Seat Number is number field - convert to number
		if (query["seat_number"])
			query["seat_number"] = Number(query["seat_number"]);

		// Prepare query for Database
		Object.keys(query).forEach((key) => {
			dbQuery[`seats.${key}`] = query[key];
		});

		const routeRes = await routeRepo.getSeatDetails(dbQuery);
		return res.status(200).send({
			status: true,
			response: routeRes || {},
		});
	} catch (err) {
		console.log(err);
		return res
			.status(err.status || 500)
			.send({ status: false, response: err.message || err });
	}
};

module.exports = {
	createRoute,
	getRoute,
	getSeatDetails,
};
