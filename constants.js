module.exports = {
	seatStatus: {
		available: "AVAILABLE",
		booked: "BOOKED",
	},
	numberOfSeats: 40,
};
